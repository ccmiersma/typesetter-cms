FROM centos/systemd

MAINTAINER "Christopher Miersma" <ccmiersma@gmail.com>

COPY Typesetter*.tar.gz /

RUN yum -y group install web-server php ; \
	yum clean all ; \
	systemctl enable httpd.service ; \
	tar xvfz /Typesetter*.tar.gz --strip-components=1 -C /var/www/html ; \
	chown -R apache:apache /var/www/html/data 

VOLUME /var/www/html/data

EXPOSE 80

CMD ["/usr/sbin/init"]
