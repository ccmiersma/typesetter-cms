appdir := /opt/typesetter-cms/app
localstatedir := /var/opt/typesetter-cms
default_site_dir := $(localstatedir)/default-site
confdir := /etc/opt/typesetter-cms

all: build

	

build:
	install -d -m 0755 build

.PHONY: check
check:
	rm -rf build/test
	make install DESTDIR=build/test

install: build
	install -dv $(DESTDIR)
	install -dv $(DESTDIR)$(appdir) $(DESTDIR)$(default_site_dir) $(DESTDIR)$(confdir) 
	tar xvfz ./Typesetter*.tar.gz --strip-components=1 -C $(DESTDIR)$(appdir)
	mv -v $(DESTDIR)$(appdir)/data $(DESTDIR)$(default_site_dir)/data
	mv -v $(DESTDIR)$(appdir)/gpconfig.php $(DESTDIR)$(confdir)/
	install -m 0644 -t $(DESTDIR)$(confdir)/ typesetter-cms.conf
	ln -s $(default_site_dir)/data $(DESTDIR)$(appdir)
	ln -s $(confdir)/gpconfig.php $(DESTDIR)$(appdir)/
	ln -s $(confdir)/gpconfig.php $(DESTDIR)$(default_site_dir)/
	ln -s $(appdir)/index.php $(DESTDIR)$(default_site_dir)/
	ln -s $(appdir)/Addon.ini $(DESTDIR)$(default_site_dir)/
	ln -s $(appdir)/robots.txt $(DESTDIR)$(default_site_dir)/
	ln -s $(appdir)/include $(DESTDIR)$(default_site_dir)/
	ln -s $(appdir)/addons $(DESTDIR)$(default_site_dir)/
	ln -s $(appdir)/themes $(DESTDIR)$(default_site_dir)/
	tar cfz $(DESTDIR)$(localstatedir)/default-site.tar.gz -C $(DESTDIR)$(default_site_dir)/ Addon.ini  addons  data  gpconfig.php  include  index.php  robots.txt  themes

clean:
	rm -rf build/

.PHONY: docker_build
docker_build:
	sudo docker build --rm --no-cache -t typesetter .

.PHONY: docker_run
docker_run:
	sudo docker run --privileged --name typesetter -v /sys/fs/cgroup:/sys/fs/cgroup:ro -v typesetter-data:/var/www/html/data:rw -p 8080:80 -d typesetter

.PHONY: docker_stop
docker_stop:
	sudo docker kill typesetter
	sudo docker rm typesetter

.PHONY: docker_clean
docker_clean:
	sudo docker volume rm typesetter-data
	sudo docker image rm typesetter
