%define author Christopher Miersma

Name:		typesetter-cms
Version:        5.1.0
Release:        2.local%{?dist}

Summary:	Typesetter CMS
Group:		Applications
License:	MIT
URL:		https://gitlab.com/ccmiersma/%{name}/
Source0:	%{name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  make
Requires:       php

%description
The Typesetter CMS.


%prep
%setup -q


%build

make

%install


%make_install

%clean
%__rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(0644,root,root, 0755)
%config(noreplace) /etc/opt/%{name}/*
/opt/%{name}
/var/opt/%{name}/default-site*
%attr(-,apache,apache) /var/opt/%{name}/default-site/data


# The post and postun update the man page database
%post

%postun

%changelog
* Fri Oct 05 2018 Christopher Miersma <ccmiersma@gmail.com> 5.1.0-2.local
- new package built with tito


